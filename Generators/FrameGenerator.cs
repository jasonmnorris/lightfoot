using System.Collections.Generic;
using System.Linq;

namespace Lights
{

    public class FrameGenerator
    {
        IPatternGenerator[] generators;
        const byte SYNC = 0x7E;
        const byte ADDRESS = 0x80;

        public FrameGenerator()
        {
            var firstSetChannelCount = 12;
            var secondSetChannelCount = 12;
            var framesPerGenerator = firstSetChannelCount * 15;
            generators = new IPatternGenerator[] {
                // new CyclingGenerator(firstSetChannelCount,
                //     (framesPerGenerator, () => new SingleLightChasingGenerator(channelCount: firstSetChannelCount)),
                //     (framesPerGenerator, () => new ChasingRingGenerator(channelCount: firstSetChannelCount, 2)),
                //     (framesPerGenerator, () => new TrailGenerator(channelCount: firstSetChannelCount, 2)),
                //     //(100, () => new RandomGenerator(channels: 10)),
                //     (framesPerGenerator, () => new PulsingGenerator(channels: firstSetChannelCount, framesPerCycle: 40))
                // ),
                new LevelCorrectingGenerator(new SingleLightChasingGenerator(channelCount: secondSetChannelCount, .8), .6),
                new DelayGenerator(
                    new SingleLightChasingGenerator(channelCount: secondSetChannelCount, .8, leaderPosition: 6),
                    channels: secondSetChannelCount,
                    framesToDelay: 10)
                // new TrailGenerator(channelCount: 10, tailLength: 5),
                // new BlankGenerator(2),
                // new TrailGenerator(channelCount: 8, tailLength: 3),
                // new PulsingGenerator(1, 3),
                // new RandomGenerator(1),
                // new RandomGenerator(1),
                // new PulsingGenerator(1, 21)
            };
        }

        public byte[] GetNextFrame()
        {
            var result = new List<byte>{
                SYNC,
                ADDRESS
            };

            foreach (var generator in generators)
            {
                var thisResult = generator.GetNextFrame();
                result.AddRange(thisResult);
            }
            return result.ToArray();
        }

    }
}