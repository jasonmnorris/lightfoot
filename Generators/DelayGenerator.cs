using System.Linq;

namespace Lights
{
	public class DelayGenerator : IPatternGenerator {
		private readonly IPatternGenerator generator;
		private readonly int channels;
		private readonly int framesToDelay;
        private int framesDelayed;

		public DelayGenerator(IPatternGenerator generator, int channels, int framesToDelay)
		{
			this.generator = generator;
			this.channels = channels;
			this.framesToDelay = framesToDelay;
		}

		public byte[] GetNextFrame()
		{
			if(framesDelayed < framesToDelay) {
                framesDelayed += 1;
                return Enumerable.Repeat(new byte(), channels).ToArray();
            }

            return generator.GetNextFrame();
		}
	}
}