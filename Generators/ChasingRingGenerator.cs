using System;

namespace Lights
{

    public class ChasingRingGenerator : IPatternGenerator
    {
        public int LeaderPosition { get; set; }
        public bool Reverse { get; set; }
        public byte[] PreviousPosition { get; set; } = null;
        public bool Increasing { get; set; } = true;

        const int INTENSITY_MULTIPLIER = 40;
        public int TailLength { get; }
        public int ChannelCount { get; }

        public ChasingRingGenerator(int channelCount, int tailLength)
        {
            ChannelCount = channelCount;
            TailLength = tailLength;
        }

        public byte[] GetNextFrame()
        {
            var result = new byte[ChannelCount];

            for (var position = 0; position < ChannelCount; position++)
            {
                result[position] = (byte)GetPositionIlluminationLevel(LeaderPosition, position);
            }

            AdjustPosition();
            return result;
        }

        public void AdjustPosition()
        {
            // if (Reverse)
            // {
            //     LeaderPosition--;
            //     if (LeaderPosition <= 0 - TailLength)
            //         Reverse = !Reverse;

            // }
            // else
            // {
                if (LeaderPosition == ChannelCount) {
                    //Reverse = !Reverse;
                    LeaderPosition = 0;
                } else {
                    LeaderPosition++;
                }
            //}
        }

        public int GetPositionIlluminationLevel(int leaderPosition, int position)
        {
            if (leaderPosition == position)
                return 255;

            var difference = GetDistanceFromLeader(position);
            if (difference < TailLength)
            {
                return 255 - (difference * INTENSITY_MULTIPLIER);
            }

            return 0;
        }

        public int GetDistanceFromLeader(int position) {
            if(LeaderPosition + TailLength > ChannelCount - 1)
                return Math.Abs(position - (LeaderPosition - ChannelCount));

            return Math.Abs(position - LeaderPosition);
        }
    }
}