using System;
using System.Linq;

namespace Lights
{
	public class LevelCorrectingGenerator : IPatternGenerator
	{
		private readonly IPatternGenerator generator;
		private readonly double correctionRatio;

		public LevelCorrectingGenerator(IPatternGenerator generator, double correctionRatio)
		{
			this.generator = generator;
			this.correctionRatio = correctionRatio;
		}

		public byte[] GetNextFrame()
		{
            var frame = generator.GetNextFrame();
            var result = frame.Select(f => Convert.ToByte(Convert.ToInt32(Math.Floor(((int)f) * correctionRatio)))).ToArray();
            return result;
		}
	}
}