using System;

namespace Lights
{

    public class SkippingGenerator : IPatternGenerator
    {
        private readonly int skipBetweenFrames;
        private readonly IPatternGenerator generator;
        private int currentFrameNumber = 0;
        private byte[] currentFrame = null;

        public SkippingGenerator(int skipBetweenFrames, IPatternGenerator generator)
        {
            this.skipBetweenFrames = skipBetweenFrames;
            this.generator = generator;
        }

        public byte[] GetNextFrame()
        {
            if(currentFrame == null)
                currentFrame = generator.GetNextFrame();

            currentFrame = generator.GetNextFrame();
            if(currentFrameNumber < skipBetweenFrames) {
                currentFrameNumber++;
                return currentFrame;
            }

            currentFrame = generator.GetNextFrame();
            currentFrameNumber = 1;
            return currentFrame;
        }
    }
}