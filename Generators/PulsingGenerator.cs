using System;
using System.Linq;

namespace Lights
{
    public class PulsingGenerator : IPatternGenerator
    {
        private readonly int _framesPerCycle;
        private readonly int _channels;
        private int _currentFrame = 0;
        private bool _reverse = false;
        const int MAX_VALUE = 255;

        public PulsingGenerator(int channels, int framesPerCycle)
        {
            this._channels = channels;
            this._framesPerCycle = framesPerCycle;
        }
        public byte[] GetNextFrame()
        {
            
            var level = GetLevel(_currentFrame);

            if (_currentFrame > _framesPerCycle)
            {
                _currentFrame--;
                _reverse = true;
            }
            else if (_currentFrame <= 0)
            {
                _currentFrame = 1;
                _reverse = false;
            }
            else
            {
                if (_reverse)
                    _currentFrame--;
                else
                    _currentFrame++;
            }

            
            return Enumerable.Repeat((byte)level, _channels).ToArray();
        }

        public int GetLevel(int _currentFrame)
        {
            var multiplier = (255 / _framesPerCycle);
            return _currentFrame * multiplier;
        }
    }
}