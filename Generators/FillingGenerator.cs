namespace Lights
{

    public class FillingGenerator : IPatternGenerator
    {
        private readonly int channels;
        private int frameNumber = 0;

        private bool filling = true;

        public FillingGenerator(int channels)
        {
            this.channels = channels;
        }
        public byte[] GetNextFrame()
        {
            var result = new byte[channels];
            var lowestToFill = frameNumber - 1;

            if(filling) {
                for(var i = 0; i < channels; i++) {
                    result[i] = (byte)(i < lowestToFill ? 255 : 0);
                }

            } else {
                for(var i = 0; i < channels; i++) {
                    result[i] = (byte)(i < lowestToFill ? 255 : 0);
                }
            }

            return result;
        }
    }
}