using System.Linq;

namespace Lights
{
    public class BlankGenerator : IPatternGenerator
    {
        private int _channelCount;
        public BlankGenerator(int channelCount)
        {
            _channelCount = channelCount;
        }
        
        public byte[] GetNextFrame()
        {
            return Enumerable.Repeat((byte)0, _channelCount).ToArray();
        }
    }
}