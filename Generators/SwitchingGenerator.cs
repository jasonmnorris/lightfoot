using System.Linq;

namespace Lights
{
    public class SwitchingGenerator : IPatternGenerator
    {
        private readonly int firstSet;
        private readonly int secondSet;
        private bool second = false;

        public SwitchingGenerator(int firstSet, int secondSet)
        {
            this.firstSet = firstSet;
            this.secondSet = secondSet;
        }

        public byte[] GetNextFrame()
        {
            byte firstValue = (byte)(second ? 0 : 255);
            byte secondValue = (byte)(second ? 255 : 0);
            second = !second;
            var result = Enumerable.Repeat(firstValue, firstSet).ToList();
            result.AddRange(Enumerable.Repeat(secondValue, secondSet));
            return result.ToArray();
        }
    }
}