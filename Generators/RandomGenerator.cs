using System;
using System.Linq;

namespace Lights
{
    public class RandomGenerator : IPatternGenerator
    {
        private readonly int channels;
        Random rand;

        public RandomGenerator(int channels)
        {
            this.channels = channels;
            rand = new Random();
        }
        public byte[] GetNextFrame()
        {
            var value = rand.Next(255);
            return Enumerable.Repeat((byte)value, channels).ToArray();
        }
    }
}