using System;
using System.Linq;

namespace Lights
{
	public class ReversingGenerator : IPatternGenerator
	{
		private readonly IPatternGenerator generator;

		public ReversingGenerator(IPatternGenerator generator)
        {
			this.generator = generator;
		}

		public byte[] GetNextFrame()
		{
			var frame = generator.GetNextFrame();
            return frame.Reverse().ToArray();
		}
	}
}