namespace Lights
{
    public interface IPatternGenerator
    {
        byte[] GetNextFrame();
    }
}