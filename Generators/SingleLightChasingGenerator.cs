using System;
using System.Linq;

namespace Lights
{
	public class SingleLightChasingGenerator : IPatternGenerator
    {
        public int LeaderPosition { get; set; }
        const int INTENSITY_MULTIPLIER = 40;
		private readonly double decayRate;

		public int ChannelCount { get; }
        
        private byte[] previousFrame;

        public SingleLightChasingGenerator(int channelCount, double decayRate, int leaderPosition = 0)
        {
            ChannelCount = channelCount;
            previousFrame = Enumerable.Repeat((byte) 0, channelCount).ToArray();
            LeaderPosition = leaderPosition;
			this.decayRate = decayRate;
		}

        public byte[] GetNextFrame()
        {
            var result = new byte[ChannelCount];

            for (var position = 0; position < ChannelCount; position++)
            {
                var calculated = (byte)GetPositionIlluminationLevel(LeaderPosition, position);
                var decayValue = Math.Floor((previousFrame[position] * decayRate));
                var positionIllumination = Math.Max((int)calculated, decayValue);
                result[position] = (byte) positionIllumination;
            }

            AdjustPosition();

            previousFrame = result;

            return result;
        }

        public void AdjustPosition()
        {
            // if (Reverse)
            // {
            //     LeaderPosition--;
            //     if (LeaderPosition <= 0 - TailLength)
            //         Reverse = !Reverse;

            // }
            // else
            // {
                if (LeaderPosition == ChannelCount - 1) {
                    //Reverse = !Reverse;
                    LeaderPosition = 0;
                } else {
                    LeaderPosition++;
                }
            //}
        }

        public int GetPositionIlluminationLevel(int leaderPosition, int position)
        {
            if (leaderPosition == position)
                return 255;

            return 0;
        }

        public int GetDistanceFromLeader(int position) {
            if(LeaderPosition > ChannelCount - 1)
                return Math.Abs(position - (LeaderPosition - ChannelCount));

            return Math.Abs(position - LeaderPosition);
        }
    }
}