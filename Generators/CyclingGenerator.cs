using System;

namespace Lights
{
    public class CyclingGenerator : IPatternGenerator
    {
        private int generatorNumber = 0;
        private int frameNumber = 0;
        private IPatternGenerator generator;
        private (int frameCount, Func<IPatternGenerator> genFunc)[] _generators;

        public CyclingGenerator(int channels, params (int frameCount, Func<IPatternGenerator> genFunc)[] generators)
        {
            generator = generators[0].genFunc();
            _generators = generators;
        }

        public byte[] GetNextFrame()
        {
            if (frameNumber >= _generators[generatorNumber].frameCount)
            {
                if (generatorNumber == _generators.Length - 1)
                {
                    generatorNumber = 0;
                    Console.WriteLine("Using " + _generators[generatorNumber].GetType().Name);
                }
                else
                {
                    generatorNumber++;
                    Console.WriteLine("Using " + _generators[generatorNumber].GetType().Name);
                }

                frameNumber = -1;
                generator = _generators[generatorNumber].genFunc();
            }
            
            frameNumber++;
            return generator.GetNextFrame();
        }
    }
}