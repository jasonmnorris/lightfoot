using System;

namespace Lights
{

    public class TrailGenerator : IPatternGenerator
    {
        public int LeaderPosition { get; set; }
        public bool Reverse { get; set; }
        public byte[] PreviousPosition { get; set; } = null;
        public bool Increasing { get; set; } = true;

        const int INTENSITY_MULTIPLIER = 40;
        public int TailLength { get; }
        public int ChannelCount { get; }

        public TrailGenerator(int channelCount, int tailLength)
        {
            ChannelCount = channelCount;
            TailLength = tailLength;
            LeaderPosition = 0 - tailLength + 1;
        }

        public byte[] GetNextFrame()
        {
            var result = new byte[ChannelCount];

            for (var position = 0; position < ChannelCount; position++)
            {
                result[position] = (byte)GetPositionIlluminationLevel(LeaderPosition, position);
            }

            AdjustPosition();
            return result;
        }

        public void AdjustPosition()
        {
            if (Reverse)
            {
                LeaderPosition--;
                if (LeaderPosition <= 0 - TailLength)
                    Reverse = !Reverse;

            }
            else
            {
                if (LeaderPosition >= ChannelCount + TailLength - 1) {
                    Reverse = !Reverse;
                    LeaderPosition--;
                } else {
                    LeaderPosition++;
                }
            }
        }

        public int GetPositionIlluminationLevel(int leaderPosition, int position)
        {
            if (leaderPosition == position)
                return 255;

            var difference = Math.Abs(position - leaderPosition);
            if (difference < TailLength)
            {
                return 255 - (difference * INTENSITY_MULTIPLIER);
            }

            return 0;
        }
    }
}