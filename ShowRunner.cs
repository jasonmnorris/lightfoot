using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Lights
{
    public class ShowRunner : IHostedService
    {
        public ShowRunner(ILogger<ShowRunner> logger, ISerial port, IConfiguration config)
        {
            this.logger = logger;
            this.port = port;
            this.frameDelayMs = Convert.ToInt32(config["frameDelayMs"]);
        }

        private bool shouldStop;
        private readonly ILogger<ShowRunner> logger;
        private readonly ISerial port;
		private readonly int frameDelayMs;

		public Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("Available Ports:");
            foreach (string s in SerialPort.GetPortNames())
            {
                logger.LogInformation("   {0}", s);
            }

            var generator = new FrameGenerator();
            
            while (true)
            {
                try
                {
                    var input = generator.GetNextFrame();
                    Debug.WriteLine(string.Join('|', input.Select(p => p.ToString())));

                    port.Write(input);
                    Thread.Sleep(frameDelayMs);
                    if (shouldStop)
                        return Task.CompletedTask;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Error Running Show");
                }

                if(cancellationToken.IsCancellationRequested)
                    return Task.CompletedTask;
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            shouldStop = true;
            Thread.Sleep(100);
            return Task.CompletedTask;
        }
    }
}