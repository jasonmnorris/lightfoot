using System;
using System.IO.Ports;

namespace Lights
{
    public class Serial : ISerial
    {
        static SerialPort _serialPort;
        public Serial(string portName)
        {
            _serialPort = new SerialPort(portName);
            Console.WriteLine($"Port set to {_serialPort.PortName}");
            _serialPort.BaudRate = 57600;
            Console.WriteLine($"Baud rate is {_serialPort.BaudRate}");

            _serialPort.ReadTimeout = 250;
            _serialPort.WriteTimeout = 250;

            _serialPort.Open();
        }

        public void Write(byte[] input)
        {
            _serialPort.Write(input, 0, input.Length);
        }
    }
}