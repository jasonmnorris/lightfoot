using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Ports;
using System.Threading;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace Lights
{
    public class Program
    {
        public static void Main()
        {
            Console.WriteLine("Starting Lightfoot...");

            Host.CreateDefaultBuilder()
                .ConfigureAppConfiguration((context, configBuilder) =>
                {
                    //configBuilder.AddJsonFile("appsettings.json");
                })
                .ConfigureServices((context, services) =>
                {
                    services
                        .AddScoped<ISerial>(serviceProvider => {
                            var port = context.Configuration["serialPort"];
                            if(string.IsNullOrWhiteSpace(port))
                                return new FakeSerial(serviceProvider.GetRequiredService<ILogger<FakeSerial>>());

                            return new Serial(port);
                        })
                        .AddHostedService<ShowRunner>()
                    ;
                })
                .Build()
                .Run()
                ;
        }
    }
}