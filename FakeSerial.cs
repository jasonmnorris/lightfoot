using System;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Lights
{
    public class FakeSerial : ISerial
    {
        private readonly ILogger<FakeSerial> logger;

        public FakeSerial(ILogger<FakeSerial> logger)
        {
            this.logger = logger;
        }
        public void Write(byte[] input)
        {
            logger.LogInformation(String.Join("", input.Select(b => b > 128 ? "*" : ".")));
        }
    }
}