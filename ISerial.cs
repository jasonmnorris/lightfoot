namespace Lights
{
    public interface ISerial
    {
        void Write(byte[] input);
    }
}